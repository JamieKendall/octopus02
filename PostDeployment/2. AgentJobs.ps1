Configuration AgentJobs {

    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullorEmpty()]
        [String] $ComputerName
    )
    $Instancename = "MSSQLSERVER"
    Import-DSCResource -ModuleName ComputerManagementDSC
    Import-DscResource -ModuleName PSDscResources

   Node $Computername {


        script CycleErrorLogJob {

            SetScript  = {
                New-DbaAgentJob -SqlInstance $using:Computername\$using:InstanceName -Job "CycleErrorLog"
                New-DbaAgentJobStep -SqlInstance $using:Computername\$Instancename -Command "EXEC sp_cycle_errorlog" -StepId 1 -StepName "sp_cycle_errorlog" -Job "CycleErrorLog" -Subsystem TransactSql -OnSuccessAction QuitWithSuccess 
            }
            TestScript = {$ExistingJobs = Get-DbaAgentJob -SqlInstance $using:Computername\$using:InstanceName | Where-Object {$_.Name -Match "CycleErrorLog"}
                if ($existingJobs) {$true} else {$false}}
            GetScript  = {return@ {Result = $ExistingJobs = Get-DbaAgentJob -SqlInstance $using:Computername\$using:InstanceName | Where-Object {$_.Name -Match "CycleErrorLog"} }}

        }

        script AddScheduletoCycleErrorLog {

            SetScript  = {
                  New-DbaAgentSchedule -SqlInstance $using:Computername\$using:InstanceName -Job "CycleErrorLog" -FrequencyType "Daily" -StartTime "000000" -Schedule "CycleErrorLog@00:00" -FrequencyInterval 1 -Force
            }
            TestScript = {$ExistingJobs = Get-DbaAgentJob -SqlInstance $using:Computername\$using:InstanceName | Where-Object {$_.Name -Match "CycleErrorLog"}
            try {$existingJobs.HasSchedule
                return $false
            }
            catch {
                return $true
            }}
            GetScript  = {return@ {Result = $ExistingJobs = Get-DbaAgentJob -SqlInstance $using:Computername\$using:InstanceName | Where-Object {$_.Name -Match "CycleErrorLog"} }}

        }


        script AddScheduletoOutputFileCleanup {

            SetScript  = {
                New-DbaAgentSchedule -SqlInstance $using:Computername\$using:InstanceName -Job "Output File Cleanup" -FrequencyType "Weekly" -FrequencyInterval "Monday" -StartTime "010000"  -Schedule "Output File Cleanup@01:00" -Force 
            }
            TestScript = {$ExistingJobs = Get-DbaAgentJob -SqlInstance $using:Computername\$using:InstanceName | Where-Object {$_.Name -Match "Output File Cleanup"}
            try {$existingJobs.HasSchedule
                return $false
            }
            catch {
                return $true
            }}
            GetScript  = {return@ {Result = $ExistingJobs = Get-DbaAgentJob -SqlInstance $using:Computername\$using:InstanceName | Where-Object {$_.Name -Match "Output File Cleanup"} }}

        }

        script AddScheduletoCommandLogCleanup {

            SetScript  = {
                New-DbaAgentSchedule -SqlInstance $using:Computername\$using:InstanceName -Job "CommandLog Cleanup" -FrequencyType "Weekly" -FrequencyInterval "Monday" -StartTime "010000" -Schedule "CommandLog Cleanup@01:00" -Force  
            }
            TestScript = {$ExistingJobs = Get-DbaAgentJob -SqlInstance $using:Computername\$using:InstanceName | Where-Object {$_.Name -Match "CommandLog Cleanup"}
            try {$existingJobs.HasSchedule
                return $false
            }
            catch {
                return $true
            }}
            GetScript  = {return@ {Result = $ExistingJobs = Get-DbaAgentJob -SqlInstance $using:Computername\$using:InstanceName | Where-Object {$_.Name -Match "CommandLog Cleanup"} }}

        }

        script AddScheduletoDatabaseIntegrityCheck-System {

            SetScript  = {
                New-DbaAgentSchedule -SqlInstance $using:Computername\$using:InstanceName -Job "DatabaseIntegrityCheck - SYSTEM_DATABASES" -FrequencyType "Daily" -StartTime "010000" -Schedule "DatabaseIntegrityCheck - SYSTEM_DATABASES@01:00"  -FrequencyInterval 1 -Force
            }
            TestScript = {$ExistingJobs = Get-DbaAgentJob -SqlInstance $using:Computername\$using:InstanceName | Where-Object {$_.Name -Match "DatabaseIntegrityCheck - SYSTEM_DATABASES"}
            try {$existingJobs.HasSchedule
                return $false
            }
            catch {
                return $true
            }}
            GetScript  = {return@ {Result = $ExistingJobs = Get-DbaAgentJob -SqlInstance $using:Computername\$using:InstanceName | Where-Object {$_.Name -Match "DatabaseIntegrityCheck - SYSTEM_DATABASES"} }}

        }

        script AddScheduletoDatabaseIntegrityCheck-User {

            SetScript  = {
                New-DbaAgentSchedule -SqlInstance $using:Computername\$using:InstanceName -Job "DatabaseIntegrityCheck - USER_DATABASES" -FrequencyType "Daily" -StartTime "020000" -Schedule "DatabaseIntegrityCheck - SYSTEM_DATABASES@02:00" -FrequencyInterval 1 -Force
            }
            TestScript = {$ExistingJobs = Get-DbaAgentJob -SqlInstance $using:Computername\$using:InstanceName | Where-Object {$_.Name -Match "DatabaseIntegrityCheck - USER_DATABASES"}
            try {$existingJobs.HasSchedule
                return $false
            }
            catch {
                return $true
            }}
            GetScript  = {return@ {Result = $ExistingJobs = Get-DbaAgentJob -SqlInstance $using:Computername\$using:InstanceName | Where-Object {$_.Name -Match "DatabaseIntegrityCheck - USER_DATABASES"} }}

        }
    }
}

AgentJobs -Computername 'OCTOPUS02'

start-DscConfiguration -path .\AgentJobs -verbose -wait -force