﻿Configuration ConfigureSQL
{
    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullorEmpty()]
        [String] $ComputerName
    )
    $Instancename = "MSSQLSERVER"
    Import-DscResource -Module SQLServerDSC
    Import-DSCResource -ModuleName ComputerManagementDSC
    Import-DscResource -ModuleName StorageDsc
    Import-DscResource -ModuleName SecurityPolicyDsc
    Import-DscResource -ModuleName PSDscResources



    Node $Computername
    {
      # Set LCM to reboot if needed
        LocalConfigurationManager {
            DebugMode          = "ForceModuleImport"
            RebootNodeIfNeeded = $true
        }

        SqlServerNetwork 'ChangeTcpIpOnDefaultInstance' {
            InstanceName   = $Instancename
            ProtocolName   = 'Tcp'
            IsEnabled      = $true
            TCPDynamicPort = $false
            TCPPort        = 1433
            RestartService = $true

        }
        
        SqlServerMemory Set_SQLServerMaxMemory_ToAuto {
            Ensure       = 'Present'
            DynamicAlloc = $True
            InstanceName = $Instancename
        }
        SqlServerMaxDop Set_SQLServerMaxDop_ToAuto {
            Ensure       = 'Present'
            DynamicAlloc = $true
            InstanceName = $InstanceName
        }
        SqlServerLogin Add_WindowsUser {
            Ensure       = 'Present'
            ServerName   = $Computername
            Name         = 'BUILTIN\Administrators'
            LoginType    = 'WindowsGroup'
            InstanceName = $InstanceName
        }

        SqlServerConfiguration ('SQLConfigCLR') {
            Servername   = $Computername
            InstanceName = $InstanceName
            OptionName   = 'clr enabled'
            OptionValue  = 1
        }
        SqlServerConfiguration ('BackupCompression') {
            Servername   = $Computername
            InstanceName = $InstanceName
            OptionName   = 'backup compression default'
            OptionValue  = 1
        }
        SqlServerConfiguration ('RemoteAdmin') {
            Servername   = $Computername
            InstanceName = $InstanceName
            OptionName   = 'remote admin connections'
            OptionValue  = 1
        }
        SqlServerConfiguration ('Costthreshold') {
            Servername   = $Computername
            InstanceName = $InstanceName
            OptionName   = 'cost threshold for parallelism'
            OptionValue  = 50
        }
        PowerPlan SetPlanHighPerformance {
            IsSingleInstance = 'Yes'
            Name             = 'High performance'
        }

        UserRightsAssignment AssignVolumeMaintenanceTaskToSQL {
            Policy   = "Perform_volume_maintenance_tasks"
            Identity = (Get-DbaService -server $computername -ServiceName $Instancename).StartName
            Force    = $true
        }

        SqlDatabase Create_Database {
            Ensure       = 'Present'
            ServerName   = $Computername
            InstanceName = $InstanceName
            Name         = 'DFI_DBA'
        }

        Service SQLAgentAutoStart {
            Name        = "SQLServerAgent"
            Ensure      = 'Present'
            StartUpType = 'Automatic'
        }

           Service SSISAutoStart
        {
        Name = 'MsDtsServer150'
        Ensure = 'Present'
        StartUpType = 'Automatic'
        }

        Script SetErrorLogFilesto31 {

            SetScript  = {
                $NumOfLogs = Connect-Dbainstance $using:Computername\$using:InstanceName
                $NumOflogs.NumberOfLogFiles = 31
                $NumOfLogs.Alter()}

            TestScript = {
                $NumOfLogs = Connect-Dbainstance $using:Computername\$using:InstanceName
                if ($NumOflogs.NumberOfLogFiles -eq 31) {$true} else {$false}}

            GetScript  = {$NumOfLogs = Connect-Dbainstance $using:Computername\$using:InstanceName
                return @{Result = $NumOflogs.NumberOfLogFiles}
            }
        }

        Script SetRecoveryMasterModel {
            SetScript  = {
                Set-DbaDbRecoveryModel -Database model -SqlInstance $using:Computername\$using:InstanceName -RecoveryModel Simple
            }

            TestScript = {
                $MasterDB = Get-DbaDbRecoveryModel -Database model -SqlInstance $using:Computername\$using:InstanceName
                if ($MasterDB.RecoveryModel -eq 'Simple') {$true} else {$false}}

            GetScript  = {$MasterDB = Get-DbaDbRecoveryModel -Database model -SqlInstance $using:Computername\$using:InstanceName
                return @{Result = $MasterDB.RecoveryModel}
            }
        }

        Script SetStartupParam {

            SetScript  = {
                Set-DbaStartupParameter -SqlInstance $using:Computername\$using:InstanceName -TraceFlag 3226 -TraceFlagOverride -Confirm:$false}

            TestScript = {
                $Startup = Get-DbaStartupParameter $using:Computername\$using:InstanceName
                if ($Startup.TraceFlags -Match "3226" ) {$true} else {$false}}

            GetScript  = {$Startup = Get-DbaStartupParameter $using:Computername\$using:InstanceName
                return @{Result = $Startup.TraceFlags}
            }
        }

        Script DisableSA {

            SetScript  = {
                Invoke-Sqlcmd -ServerInstance $using:Computername\$using:InstanceName -Database master -Query "EXEC sp_SetAutoSAPasswordAndDisable"}

            TestScript = {$SA = get-dbalogin -SqlInstance $using:Computername\$using:InstanceName -Login sa
                If ($SA.IsDisabled) {$true} else {$false}
            }

            GetScript  = {$SA = get-dbalogin -SqlInstance $using:Computername\$using:InstanceName -Login sa
                return @{Result = $SA.ID}
            }
        }

        Script ConfigureTempDB {

            SetScript  = {
                #Dynamic Setting
                $TotalSpace = Get-DbaDiskSpace -ComputerName $using:Computername\$using:InstanceName|Where-object {$_.Label -eq "TempDB"} |Select-Object Capacity
                [int]$WorkingSpace = ($Totalspace.Capacity).Megabyte - 1000 # this 1000MB is to allow for file slack
                If ((Get-DbaComputerSystem -ComputerName $using:Computername\$using:InstanceName).NumberLogicalProcessors -le 3) {
                    [int]$logSpace = $Workingspace / 3
                }
                else {
                    [int]$logSpace = $Workingspace / (Get-DbaComputerSystem -ComputerName $using:Computername\$using:InstanceName).NumberLogicalProcessors
                }
                [int]$DBSpace = $WorkingSpace - $logSpace
                Set-DbaTempdbConfig -SqlInstance $using:Computername\$using:InstanceName -DisableGrowth -DataFileSize $DBSpace -LogFileSize $logSpace
                
                <#Static TempDB
                Set-DbaTempdbConfig -SqlInstance $using:Computername\$using:InstanceName -DisableGrowth -DataFileSize 240000 -LogFileSize 60000#>
            }

            TestScript = {$FreeSpace = (Get-DbaDiskSpace -ComputerName $using:Computername\$using:InstanceName|Where-object {$_.Label -eq "TempDB"}).Free.MegaByte
                if ($Freespace -gt 1000) {$false} else {$true}
            }

            GetScript  = {$Startup = Test-dbaTempDBConfig $using:Computername\$using:InstanceName
                return @{Result = $Startup.isBestPractice}
            }
        }


        script InstallMaintenanceSolution {
            SetScript  = {Install-DbaMaintenanceSolution -SqlInstance $using:Computername\$using:InstanceName -Database DFI_DBA -ReplaceExisting -InstallJobs
            }
            TestScript = {$ola = (get-dbamodule -sqlinstance $using:Computername\$using:InstanceName -database DFI_DBA |where-object name -like 'CommandExecute') -and (Get-DbaAgentJob -sqlinstance $using:Computername\$using:InstanceName -Job "CommandLog Cleanup")
                if ($ola) {$true} else {$false}}
            GetScript  = {$ola = (get-dbamodule -sqlinstance $using:Computername\$using:InstanceName -database DFI_DBA |where-object name -like 'CommandExecute') -and (Get-DbaAgentJob -sqlinstance $using:Computername\$using:InstanceName -Job "CommandLog Cleanup")
                return @{Result = $ola.Name}
            }
        }

        script WhoisActive {
            SetScript  = { Install-DbaWhoIsActive -SqlInstance $using:Computername\$using:InstanceName -Database DFI_DBA
            }
            TestScript = {$Who = get-dbamodule -SqlInstance $using:Computername\$using:InstanceName -Database DFI_DBA |where-object name -like 'sp_WhoIsActive'
                if ($Who) {$true} else {$false}}
            GetScript  = {$Who = get-dbamodule -SqlInstance $using:Computername\$using:InstanceName -Database DFI_DBA |where-object name -like 'sp_WhoIsActive'
                return @{Result = $Who.Name}
            }
        }

        script InstallFirstResponder {
            SetScript  = { Install-DbaFirstResponderKit -SqlInstance $using:Computername\$using:InstanceName -Database DFI_DBA
            }
            TestScript = {$Blitz = get-dbamodule -SqlInstance $using:Computername\$using:InstanceName -Database DFI_DBA |where-object name -like 'sp_Blitz'
                if ($Blitz) {$true} else {$false}}
            GetScript  = {$Blitz = get-dbamodule -SqlInstance $using:Computername\$using:InstanceName -Database DFI_DBA |where-object name -like 'sp_Blitz'
                return @{Result = $Blitz.Name}
            }
            
        }

        <#Script DeploySSIS {
            SetScript = {
                [Reflection.Assembly]::LoadWithPartialName("System.Web")
                $secpasswd = ConvertTo-SecureString $( [system.web.security.membership]::GeneratePassword(22, 3) ) -AsPlainText -Force
                [Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Management.IntegrationServices")
                # Store the IntegrationServices Assembly namespace to avoid typing it every time  
                $ISNamespace = "Microsoft.SqlServer.Management.IntegrationServices"
                # Create a connection to the server
                $sqlConnectionString = "Data Source=localhost;Initial Catalog=master;Integrated Security=SSPI;"
                $sqlConnection = New-Object System.Data.SqlClient.SqlConnection $sqlConnectionString
                # Create the Integration Services object
                $integrationServices = New-Object $ISNamespace".IntegrationServices" $sqlConnection
                # Provision a new SSIS Catalog
                $catalog = New-Object $ISNamespace".Catalog" ($integrationServices, "SSISDB", $secpasswd)
                $catalog.Create()
        }

            TestScript = {
                if(get-dbadatabase -SqlInstance $env:Computername -database SSISDB) {$true} else {$false}
            }

            GetScript = {
                $ssis = get-dbadatabase -SqlInstance $env:Computername -database SSISDB
                return @{Result = $ssis.name}
            }
        }#>
    }

}

ConfigureSQL -computername 'OCTOPUS02'

Start-DSCConfiguration -Path .\ConfigureSQL -wait -verbose -force


