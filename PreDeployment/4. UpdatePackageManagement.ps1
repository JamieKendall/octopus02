    $SetupScript = [Scriptblock]::Create(@"
        Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
        Install-Module PackageManagement -Force -scope Allusers
        Set-NetFirewallProfile -Enabled False
        exit
"@)

    $NewSess = New-PsSession -Computername "OCTOPUS02"
    Invoke-Command -ScriptBlock $SetupScript -Session $NewSess