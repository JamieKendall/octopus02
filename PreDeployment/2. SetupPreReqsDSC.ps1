Configuration SetupPreReqs
{
    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullorEmpty()]
        [String] $ComputerName
    )
    Import-DSCResource -Modulename PSDesiredStateConfiguration
    Import-DscResource -ModuleName PackageManagement -moduleversion 1.4.4
    Import-DSCResource -ModuleName StorageDsc 
    Import-DSCResource -ModuleName NetworkingDSC
    Import-DscResource -ModuleName Chocolatey

    Node $Computername
    {
        WindowsFeature 'NetFramework45' {
            Name   = 'NET-Framework-45-Core'
            Ensure = 'Present'
        }

        WindowsFeature 'RSAT-AD-PowerShell' {
            Name   = 'RSAT-AD-PowerShell'
            Ensure = 'Present'
        }

        WindowsFeature 'RSAT-ADDS-tools' {
            Name   = 'RSAT-ADDS-tools'
            Ensure = 'Present'
        }

        PackageManagementSource PSGallery {
            Ensure             = "Present"
            Name               = "psgallery"
            ProviderName       = "PowerShellGet"
            SourceLocation     = "https://www.powershellgallery.com/api/v2/"
            InstallationPolicy = "Trusted"
        }

        PackageManagementSource DFIGallery
        {
            Ensure         = "Present"
            Name           = "DFIGallery"
            ProviderName= "Nuget"
            SourceLocation = "http://devbuild02:1985/nuget"
            InstallationPolicy ="Trusted"
        }

        PackageManagement DFIMSA {
            Ensure    = "Present"
            Name      = "DFIMSA"
            Source    = "DFIGallery"
            DependsOn = "[PackageManagementSource]DFIGallery"
        }

        PackageManagement DFIDBA {
            Ensure    = "Present"
            Name      = "DFIDBA"
            Source    = "DFIGallery"
            DependsOn = "[PackageManagementSource]DFIGallery"
        }

        
        PackageManagement PowerShellModule {
            Ensure    = "Present"
            Name      = "PowerShellModule"
            Source    = "DFIGallery"
            DependsOn = "[PackageManagementSource]DFIGallery"
        }

        PackageManagement DbaTools {
            Ensure    = "Present"
            Name      = "DBATools"
            Source    = "PSGallery"
            DependsOn = "[PackageManagementSource]PSGallery"
        }

        PackageManagement SQLServer {
            Ensure    = "Present"
            Name      = "SQLServer"
            Source    = "PSGallery"
            DependsOn = "[PackageManagementSource]PSGallery"
        }

        PackageManagement StorageDSC {
            Ensure    = "Present"
            Name      = "StorageDSC"
            Source    = "PSGallery"
            DependsOn = "[PackageManagementSource]PSGallery"
        }

        PackageManagement PSFramework {
            Ensure    = "Present"
            Name      = "PSFramework"
            Source    = "PSGallery"
            DependsOn = "[PackageManagementSource]PSGallery"
        }

        PackageManagement PSDscResources {
            Ensure    = "Present"
            Name      = "PSDscResources"
            Source    = "PSGallery"
            DependsOn = "[PackageManagementSource]PSGallery"
        }

                PackageManagement ComputerManagementDSC {
            Ensure    = "Present"
            Name      = "ComputerManagementDSC"
            Source    = "PSGallery"
            DependsOn = "[PackageManagementSource]PSGallery"
        }

        PackageManagement SecurityPolicyDsc {
            Ensure    = "Present"
            Name      = "SecurityPolicyDsc"
            Source    = "PSGallery"
            DependsOn = "[PackageManagementSource]PSGallery"
        }

        PackageManagement SQLServerDSC {
            Ensure    = "Present"
            Name      = "SQLServerDSC"
            Source    = "PSGallery"
            DependsOn = "[PackageManagementSource]PSGallery"
        }

        PackageManagement NetworkingDSC {
            Ensure    = "Present"
            Name      = "NetworkingDSC"
            Source    = "PSGallery"
            DependsOn = "[PackageManagementSource]PSGallery"
        }
        PackageManagement Chocolatey {
            Ensure    = "Present"
            Name      = "Chocolatey"
            Source    = "PSGallery"
            DependsOn = "[PackageManagementSource]PSGallery"
        }
        }
}

Configuration SetupChoco
{
    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullorEmpty()]
        [String] $ComputerName
    )
    Import-DscResource -ModuleName Chocolatey

    Node $Computername
    {
                ChocolateySoftware Chocolatey {
            Ensure = "Present"
        }
    }
}

SetupPreReqs -Computername 'OCTOPUS02'
Start-DscConfiguration -path .\SetupPreReqs -verbose -wait -force
SetupChoco -Computername 'OCTOPUS02'
Start-DscConfiguration -path .\SetupChoco -verbose -wait -force

