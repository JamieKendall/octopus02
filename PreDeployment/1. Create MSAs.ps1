import-module ActiveDirectory
import-module dbatools
import-module PSFramework
import-module DFIMSA
$computername = 'OCTOPUS02'
$svcid = get-nextsvcid
Add-GMSASQL -options SQL, AGT, IS -id ($svcid)  -Server $Computername 
Write-PSFMessage -Level Host -Message "MSA Created"
Set-SPNPermissions -id ($svcid)
Write-PSFMessage -Level Host -Message "SPN Updated"
Add-ADGroupMember -Identity 'SQLBackup_Users' -Members "SQLSVC$svcid$"
Write-PSFMessage -Level Host -Message "SQLSVC$svcid$ Added to SQLBackup_Users"


