$Sourceiso = '\\dr-foster.lan\Filestorage\MicrosoftSoftware\MSDN\Server\SQL 2019 RC1\SQLServer2019RC-x64-ENU.iso'
#$Sourceiso = '\\dr-foster.lan\Filestorage\MicrosoftSoftware\Volume Licensing\Server\sql 2017\SW_DVD9_NTRL_SQL_Svr_Standard_Edtn_2017_64Bit_English_OEM_VL_X21-56945.ISO'
#$Sourceiso = '\\dr-foster.lan\Filestorage\MicrosoftSoftware\Volume Licensing\Server\sql 2017\SW_DVD9_SQL_Svr_Enterprise_Edtn_2017_64Bit_English_MLF_X21-56962.ISO'
#$Sourceiso = '\\dr-foster.lan\Filestorage\MicrosoftSoftware\Volume Licensing\Server\sql 2016\SW_DVD9_NTRL_SQL_Svr_Standard_Edtn_2016w_SP2_64Bit_English_OEM_VL_X21-59522.ISO'
#$Sourceiso = '\dr-foster.lan\Filestorage\MicrosoftSoftware\Volume Licensing\Server\sql 2016\Enterprise\SW_DVD9_NTRL_SQL_Svr_Ent_Core_2016w_SP2_64Bit_English_OEM_VL_X21-59533.ISO'
#$Sourceiso = 'Put Location of ISO here so it is copied to target machine'

Function DeployPreReqs {
    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullorEmpty()]
        [String] $Computername
    )
   
    $target = "\\$ComputerName\e$\Install\"
    if (!(test-path -Path "\\$Computername\e$\Install")) {
        New-Item -Path "\\$Computername\e$\Install" -ItemType Directory
    }
    $file = $SourceIso
    $filename = Split-path $file -leaf
    if (!(test-path -Path $target\$filename)){
        Copy-item -path $file -Destination $target
    }
    if (!(Get-DbaRegServer -SqlInstance DBAPROD02 -ServerName $Computername -Group AllServers)) {
        Add-DbaRegServer -SQLInstance DBAPROD02 -ServerName $computername -Group AllServers
    }
}


DeployPreReqs -Computername 'OCTOPUS02'