$computername = 'OCTOPUS02'
Try {
    $filter = "Description -like ""$computername*"""
    $svcname = Get-ADServiceAccount -Filter ("$filter")|where-object {$_.name -like "SQL*"} -ErrorAction Stop
    $svcid = $svcname.name.Substring($svcname.name.Length - 3)
}
catch {
    Write-output "MSA not Defined for this server"
    break
}
s:\setup.exe `
    /ACTION="Install" `
    /QUIET="True" `
    /UPDATEENABLED="False" `
    /INSTANCENAME="MSSQLSERVER" `
    /INSTANCEID="MSSQLSERVER" `
    /ENU="True" `
    /IACCEPTSQLSERVERLICENSETERMS="True" `
    /SQLCOLLATION="Latin1_General_CI_AS" `
    /FEATURES=SQLENGINE, IS `
    /SQLBACKUPDIR="E:\Backup\" `
    /SQLUSERDBDIR="G:\DataFiles" `
    /SQLTEMPDBDIR="I:\TempDB\" `
    /SQLUSERDBLOGDIR="F:\LogFiles" `
    /INSTALLSHAREDDIR="E:\Program Files\Microsoft SQL Server" `
    /INSTALLSHAREDWOWDIR="E:\Program Files (x86)\Microsoft SQL Server" `
    /INSTANCEDIR="E:\Program Files\Microsoft SQL Server" `
    /SQLSVCACCOUNT="Dr-Foster\SQLSVC$SVCID$" `
    /SQLSYSADMINACCOUNTS="Dr-Foster\DBAs" "NT AUTHORITY\SYSTEM" `
    /SQLSVCSTARTUPTYPE="Automatic" `
    /AGTSVCACCOUNT="Dr-Foster\AGTSVC$SVCID$" `
    /AGTSVCSTARTUPTYPE="Automatic" `
    /SQLSVCINSTANTFILEINIT="True" `
    /ISTELSVCACCT="NT Service\SSISTELEMETRY150" `
    /ISTELSVCSTARTUPTYPE="Automatic" `
    /ISSVCSTARTUPTYPE="Automatic" `
    /ISSVCACCOUNT="Dr-Foster\ISSVC$SVCID$"