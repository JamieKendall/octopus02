Configuration DisMount2019ISO
{
    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullorEmpty()]
        [String] $ComputerName
    )
    Import-DSCResource -Modulename PSDesiredStateConfiguration
    Import-DSCResource -ModuleName StorageDsc 

    Node $Computername
    {

        MountImage ISO {
            ImagePath = "E:\INSTALL\SQLServer2019RC-x64-ENU.iso"
            Ensure    = 'Absent'
        }
    }
}


DisMount2019ISO -Computername 'ETLPRODSSIS01'
Start-DscConfiguration -path .\DisMount2019ISO -verbose -wait -force