Configuration Add-2057Folder {

    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullorEmpty()]
        [String] $ComputerName
    )
    Import-DSCResource -ModuleName PSDesiredStateConfiguration
    Node $Computername {

        File odbcrll{
        DestinationPath = "E:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Binn\Polybase\ODBC Drivers\PolyBase ODBC Driver for SQL Server\2057\msodbcsqlr17.rll"
        SourcePath = "E:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Binn\Polybase\ODBC Drivers\PolyBase ODBC Driver for SQL Server\1033\msodbcsqlr17.rll" 
        Ensure = "Present"
        Type = "File"
        Checksum = "modifiedDate"
        Force = $true
    }
}
}

Add-2057Folder -ComputerName 'Enter Computername Here'

start-DSCConfiguration -Path .\Add-2057Folder -wait -verbose -force