Configuration Mount2019ISO
{
    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullorEmpty()]
        [String] $ComputerName
    )
    Import-DSCResource -Modulename PSDesiredStateConfiguration
    #Import-DscResource -ModuleName PSDscResources
    Import-DSCResource -ModuleName StorageDsc 

    Node $Computername
    {
        WindowsFeature 'NetFramework45' {
            Name   = 'NET-Framework-45-Core'
            Ensure = 'Present'
        }

        WindowsFeature 'RSAT-AD-PowerShell' {
            Name   = 'RSAT-AD-PowerShell'
            Ensure = 'Present'
        }

        WindowsFeature 'RSAT-ADDS-tools' {
            Name   = 'RSAT-ADDS-tools'
            Ensure = 'Present'
        }

        MountImage ISO {
            ImagePath   = "E:\INSTALL\SQLServer2019RC-x64-ENU.iso"
            DriveLetter = 'S'
            StorageType = 'ISO'
        }
        WaitForVolume WaitForISO {
            DriveLetter      = 'S'
            RetryIntervalSec = 5
            RetryCount       = 10
            DependsOn        = '[MountImage]ISO'
        }
    }
}


Mount2019ISO -Computername 'OCTOPUS02
'
Start-DscConfiguration -path .\Mount2019ISO -verbose -wait -force